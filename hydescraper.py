import requests 
from bs4 import BeautifulSoup
import csv

#Request webpage content
result = requests.get('https://www.worldometers.info/world-population/world-population-by-year/')

#Save content in variable
src = result.content

#Activate soup
soup = BeautifulSoup(src,'lxml')

#csv first line
firstline = ['sep=,']

#Save table in csv
tbl = soup.find('table')
header = ['Year','World population']

with open('worldpopulation.csv','w', newline='') as f:
    writer = csv.writer(f)
    for tr in tbl('tr'):
        row = [t.get_text(strip=True) for t in tr(['td', 'th'])]
        cell1 = row[0]
        cell2 = row[1].replace(',','')
        #cell3 = row[2].replace(' %','')
        #cell4 = row[3].replace(',','')
        #cell5 = row[4]
        #cell6 = row[5].replace(',','')
        #cell7 = row[6].replace(' %','')
        row = [cell1, cell2]
        writer.writerow(row)


