The data set shows yearly population and its growth.

## Data
The data is sourced from: https://www.worldometers.info/world-population/world-population-by-year/

The repo includes:
* datapackage.json
* worldpopulation.csv
*  hydescraper.py

## Preparation

***Requires:***
1. Python
2. Visualstudio / Jupyter notebook / or any platform that works with python.

***worldpopulation.csv***
* The CSV Data has 2 colummns. The first column shows "Year" since -5000 and the second column shows "World Population".

***datapackage.json***
* The json file has more detailed information such as name, licence, a graph integrated.

***hydescraper.py***
* This script will scrape the table everytime you need to scrape. 

***Instructions:***
* Copy script
* Open in jupyter notebook or python shell.
* Run
* Csv file will be saved in your main document.

## Licence
This Data Package is made available under the Public Domain Dedication and License v1.0 
